package com.example.animation1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView

class Activity1 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity1)
        val lotuslogo: ImageView = findViewById(R.id.lotuslogo) // объявляем переменную и связываем её с картинкой из xml.
        val lotuslogo2: ImageView = findViewById(R.id.lotuslogo2)

        val animalpha: Animation = AnimationUtils.loadAnimation(this, R.anim.alphaanim) // объявляем переменную, тип Animation и связываем её с файлом myanim.xml и действиями из него.
        val animrotation: Animation = AnimationUtils.loadAnimation(this, R.anim.rotateanim)
        val animscale: Animation = AnimationUtils.loadAnimation(this, R.anim.scaleanim)
        val animtranslate: Animation = AnimationUtils.loadAnimation(this, R.anim.translateanim)
        val animcombo: Animation = AnimationUtils.loadAnimation(this, R.anim.comboanim)
        // animcombo.start и stop и reset и еще куча всяких контекстных команд.
        lotuslogo.startAnimation(animcombo) // применяем аниамцию к картинке.
        lotuslogo2.startAnimation(animtranslate)
    }

    fun to_second(view: View) {
        val intent = Intent (this, Activity2::class.java)
        startActivity(intent)
    }


}