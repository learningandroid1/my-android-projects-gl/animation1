package com.example.animation1

import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity

class Activity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity2)

        val lotuslogo3: ImageView = findViewById(R.id.lotuslogo3)
        val knopochka: Button = findViewById(R.id.knopochka)
        val animcombo: Animation = AnimationUtils.loadAnimation(this, R.anim.comboanim)
        knopochka.setOnClickListener{
//            lotuslogo3.visibility = View.GONE
        lotuslogo3.startAnimation(animcombo)
        }
            // есть anim.setAnimationListener{} которому можно прикрутить например переход на другой экран по окончании анимации.
            // но там внутри него надо имплементировать методы, типа AnimationStart, и\или AnimationEnd.
    }
}